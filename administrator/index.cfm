<!--- ********************************************************************* --->
<!--- Kakapo 1.00 Released under the GNU licenses 3                         --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfprocessingdirective pageEncoding="utf-8">
<cfsilent>
<cfinclude template="system/gatekeeper.cfm">
<cfinclude template="system/defaults.cfm">
<cfinclude template="settings.cfm">
</cfsilent>
<cfinclude template="system/header.cfm">
<cfinclude template="bin/index.cfm">
<cfinclude template="system/footer.cfm">