<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2012                                         --->
<!--- ********************************************************************* --->
<cfquery name="this.q" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select * from #kakapo.table# where bugid = #url.id#
</cfquery>
<cfwddx action="cfml2wddx" input="#this.q#" output="kakapoq">

<cfif FileExists("#kakapo.LogFolder#/variables/#this.q.Code#.cfm")>
	<cffile action="read" file="#kakapo.LogFolder#/variables/#this.q.Code#.cfm" variable="kakapoFile" />
	<cfset kakapov = ungzip(trim( replacenocase(kakapoFile,'<cfabort />','') ),kakapo.compress,this.q.code)>
</cfif>

<cfif FileExists("#kakapo.LogFolder#/pages/#this.q.Code#.cfm")>
	<cffile action="read" file="#kakapo.LogFolder#/pages/#this.q.Code#.cfm" variable="code">
	<cfset kakapocode	= ungzip(trim( replacenocase(code,'<cfabort />','') ),kakapo.compress,this.q.Code)>
</cfif>
<cfoutput>
<cfsavecontent variable="kakapoexport"><kakapo>
<query><![CDATA[#tostring(kakapoq)#]]></query>
<vari><![CDATA[#tostring(kakapov)#]]></vari>
<code><![CDATA[#tostring(kakapocode)#]]></code>
</kakapo></cfsavecontent>
</cfoutput>

<cffile
	action 	= "write"
	file 	= "#kakapo.LogFolder#/variables/kakapo_#url.id#.xml"
	output 	= "#kakapoexport#">
	
<CFHEADER NAME="Content-Disposition" VALUE="attachment; filename=kakapo_#url.id#.xml">
<cfcontent file="#kakapo.LogFolder#/variables/kakapo_#url.id#.xml" type="application/x-unknown" deletefile="yes"> 