<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 27, 2011                                         --->
<!--- ********************************************************************* --->
<cfimport 
	taglib =	"../system/ct"  
	prefix =	"ct">
<ct:quickCheck>

<cfquery name="thisE" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
	select code,errorid,template,message from #kakapo.table# where bugid = #url.id#
</cfquery>

<div style="padding-left:10px; padding-right:10px">
<cfif FileExists("#kakapo.LogFolder#/variables/#thisE.Code#.cfm")>
	<!--- ****************************************** --->
	<!--- Read the log file and convert to variables --->
	<!--- ****************************************** --->
	<cffile action="read" file="#kakapo.LogFolder#/variables/#thisE.Code#.cfm" variable="kakapoFile" />
	<cfset kakapoFile = ungzip(trim( replacenocase(kakapoFile,'<cfabort />','') ),kakapo.compress,ThisE.code)>

	<cfwddx action="wddx2cfml" input="#kakapoFile#" output="kakapoXML">
	<cfset StructDelete(variables,'kakapoFile')>
	<!--- ****************************************** --->
	<!--- Tabs                                       --->
	<!--- ****************************************** --->
	<div class="tabline">
		<div>Message</div>
		<cfif StructKeyExists(kakapoXML,'session')	and not StructIsEmpty(kakapoXML.session)><div>Session</div></cfif>
		<cfif StructKeyExists(kakapoXML,'request')	and not StructIsEmpty(kakapoXML.request)><div>Request</div></cfif>
		<cfif StructKeyExists(kakapoXML,'cgi')		and not StructIsEmpty(kakapoXML.cgi)><div>CGI</div></cfif>
		<cfif StructKeyExists(kakapoXML,'cookie')	and not StructIsEmpty(kakapoXML.cookie)><div>Cookie</div></cfif>
		<cfif StructKeyExists(kakapoXML,'url')		and not StructIsEmpty(kakapoXML.url)><div>URL</div></cfif>
		<cfif StructKeyExists(kakapoXML,'form')		and not StructIsEmpty(kakapoXML.form)><div>Form</div></cfif>
		<cfif StructKeyExists(kakapoXML,'vari')		and not StructIsEmpty(kakapoXML.vari)><div>Variables</cfif></div>
		<cfif StructKeyExists(kakapoXML,'query')		and not StructIsEmpty(kakapoXML.vari)><div>Queries</cfif></div>
		<cfif FileExists("#kakapo.LogFolder#/pages/#thisE.Code#.cfm")>
			<cffile action="read" file="#kakapo.LogFolder#/pages/#thisE.Code#.cfm" variable="code">
			<cfset code	= ungzip(trim( replacenocase(code,'<cfabort />','') ),kakapo.compress,ThisE.code)>
			<cfif IsDefined("kakapoXML.vari.kakapoLog.line")>
				<cfset line	= kakapoXML.vari.kakapoLog.line>
			<cfelse>
				<cfset line	= 0>
			</cfif>
			<div>Code</div>
		</cfif>
		<div>Action</div>
	</div>
	<!--- ****************************************** --->
	<!--- Tab Boxes                                  --->
	<!--- ****************************************** --->
	<div class="tabboxes">
		<div class="box"><cfinclude template="errormsg.cfm"></div>
		<cfif StructKeyExists(kakapoXML,'session') and not StructIsEmpty(kakapoXML.session)>
			<div class="box"><cfdump var="#kakapoXML.session#"></div>
		</cfif>
		<cfif StructKeyExists(kakapoXML,'request') and not StructIsEmpty(kakapoXML.request)>
			<div class="box"><cfdump var="#kakapoXML.request#"></div>
		</cfif>
		<cfoutput>
		<cfif StructKeyExists(kakapoXML,'cgi') and not StructIsEmpty(kakapoXML.cgi)>
		<div class="box">
		<table class="vartbl" cellpadding="0" cellspacing="0" border="0">
			<cfloop collection="#kakapoXML.cgi#" item = "i">
				<tr><td class="vname" valign="top">#i#</td>
				<td class="vdata" valign="top">#kakapoXML.cgi[i]#</td></tr>
			</cfloop>
		</table>
		</div>
		</cfif>
		<cfif StructKeyExists(kakapoXML,'cookie') and not StructIsEmpty(kakapoXML.cookie)>
			<div class="box">
				<table class="vartbl" cellpadding="0" cellspacing="0" border="0">
					<cfloop collection="#kakapoXML.cookie#" item = "i">
						<tr><td class="vname" valign="top">#i#</td>
						<td class="vdata" valign="top">#kakapoXML.cookie[i]#</td></tr>
					</cfloop>
				</table>
			</div>
		</cfif>
		<cfif StructKeyExists(kakapoXML,'url') and not StructIsEmpty(kakapoXML.url)>
			<div class="box">
				<table class="vartbl" cellpadding="0" cellspacing="0" border="0">
					<cfloop collection="#kakapoXML.url#" item = "i">
						<tr><td class="vname" valign="top">#i#</td>
						<td class="vdata" valign="top">#kakapoXML.url[i]#</td></tr>
					</cfloop>
				</table>
			</div>
		</cfif>
		<cfif StructKeyExists(kakapoXML,'form') and not StructIsEmpty(kakapoXML.form)>
			<div class="box">
				<table class="vartbl" cellpadding="0" cellspacing="0" border="0">
					<cfloop collection="#kakapoXML.form#" item = "i">
						<tr><td class="vname" valign="top">#i#</td>
						<td class="vdata" valign="top">#kakapoXML.form[i]#</td></tr>
					</cfloop>
				</table>
			</div>
		</cfif>
		<cfflush>
		<cfif StructKeyExists(kakapoXML,'vari') and not StructIsEmpty(kakapoXML.vari)>
		<div class="box">
			<cfif StructKeyExists(kakapoXML.vari,'kakapoLog')>
				<cfset StructDelete(kakapoXML.vari,'kakapoLog')>
			</cfif>
			<cfdump var="#kakapoXML.vari#">
		</div>
		</cfif>
		<cfflush>
		<cfif StructKeyExists(kakapoXML,'query') and not StructIsEmpty(kakapoXML.query)>
		<div class="box">
			<cfdump var="#kakapoXML.query#">
		</div>
		</cfif>
		<cfflush>
		<cfif FileExists("#kakapo.LogFolder#/pages/#thisE.Code#.cfm")>
			<cfset Color = CreateObject("Component", "Color")>
			<div class="box"><div style="border:1px solid ##E4E4E4">#Color.colorString(code,true,line)#</div></div>
		</cfif>
		</cfoutput>
		
		<div class="box">
			<cfquery name="thisdetail" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
				select  min(etime) as etime, max(status) as status, count(errorid) as ecount from #kakapo.table# where errorid = #thisE.errorid#
			</cfquery>
			<cfoutput>
			<br />
			<cfif thisdetail.ecount gt 1>
				This Error Occered <a href="index.cfm?action=incidence&errorid=#thisE.errorid#&id=#url.id#">#thisdetail.ecount# times</a> since #dateformat(thisdetail.etime,'ddd mm/dd/yy')# #timeformat(thisdetail.etime,'hh:mm tt')#. <a href="javascript:conf('Delete Record','Are You sure You want to delete these records?','index.cfm?action=delete&errorid=#thisE.errorid#&page=#url.page#')"><img src="images/bin.png" border="0" align="absmiddle" /> Delete These Reccords.</a>
			<cfelse>
				This Error Occered at #dateformat(thisdetail.etime,'ddd mm/dd/yy')# #timeformat(thisdetail.etime,'hh:mm tt')#. <a href="javascript:conf('Delete Record','Are You sure You want to delete this record?','index.cfm?action=delete&errorid=#thisE.errorid#&page=#url.page#')"><img src="images/bin.png" border="0" align="absmiddle" /> Delete This Record.</a>
			</cfif>
			<a href="index.cfm?action=export&errorid=#thisE.errorid#&id=#url.id#"><img src="images/settingssml.png" title="Export" border="0" align="absmiddle" /> Export This Record</a>
			</cfoutput>
			
			<!--- ******************************************** --->
			<!--- Other errors in the same template            --->
			<!--- ******************************************** --->
			<cfquery name="template" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
				select  max(bugid) as id, message, line, max(status) as status, errorid from #kakapo.table# 
				where template = '#thisE.template#' and errorid <> #val(thisE.errorid)#
				group by errorid,message, line
				order by id desc 
			</cfquery>
	
			<cfif template.recordCount>
			<div style="padding:3px; margin-top:20px; clear:both; border-bottom:1px dotted #CCC; font-weight:bold;">Other Errors Occurred in This Template</div>
				<cfoutput query="template">
				<div style="padding:2px"><img src="images/#val(template.status)#_stat.png" /> <a href="index.cfm?action=incidence&errorid=#template.errorid#&id=#template.id#">#template.message# Line #template.line#</a></div>
				</cfoutput>
			</cfif>

			<!--- ******************************************** --->
			<!--- same message in other templates              --->
			<!--- ******************************************** --->
			<cfquery name="other" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
				select max(bugid) as id, template, line, max(status) as status , errorid
				from #kakapo.table# where message = '#thisE.message#' and errorid <> #val(thisE.errorid)# 
				group by errorid, template, line
				order by id desc 
			</cfquery>
			<cfif other.recordCount>
			<div style="padding:3px; margin-top:20px; clear:both; border-bottom:1px dotted #CCC; font-weight:bold;">"<cfoutput>#thisE.message#</cfoutput>" in Other Templates: </div>
			<cfoutput query="other">
				<div style="padding:2px"><img src="images/#val(other.status)#_stat.png" /> <a href="index.cfm?action=incidence&errorid=#other.errorid#&id=#other.id#">#other.template# Line #other.line#</a></div>
			</cfoutput>
			</cfif>
		</div>
	</div>

<cfelse>
	<cfset session.msg	= "No Error Log Available.">
	<br /><ct:msg><br /><br />
	<cfoutput><a href="javascript:conf('Delete Record','Are You sure You want to delete this record?','index.cfm?action=delete&errorid=#thisE.errorid#&page=#url.page#')"><img src="images/bin.png" border="0" align="absmiddle" /> Delete This Record.</a></cfoutput>
</cfif>
</div>