<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->
<cfimport 
	taglib =	"../system/ct"  
	prefix =	"ct">
<ct:quickCheck>
<!--- *************************************************** --->
<!--- Mark Record as Viewed                               --->
<!--- *************************************************** --->
<cfif val(url.id)>
	<cfquery name="update" datasource="#kakapo.datasource#" username="#kakapo.username#" password="#kakapo.password#">
		update #kakapo.table# set viewed = <cfqueryPARAM value = "1" CFSQLType = "CF_SQL_BIT"> 
		where bugid = <cfqueryPARAM value = "#val(url.id)#" CFSQLType = "CF_SQL_INTEGER">
	</cfquery>
</cfif>