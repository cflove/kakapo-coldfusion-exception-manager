<!--- ********************************************************************* --->
<!--- Kakapo 1.00  Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : Aug 27, 2012                                            --->
<!--- ********************************************************************* --->
<cfprocessingdirective pageEncoding="utf-8">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head><link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="js/jquery/css/pepper-grinder/jquery-ui-1.8.custom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<script type='text/javascript' src='js/jquery/jquery-1.4.2.min.js'></script>
<script type='text/javascript' src='js/jquery/jquery-ui-1.8.custom.min.js'></script>
<script type='text/javascript' src='js/jquery/jquery.tipsy.js'></script>
<script type='text/javascript' src='js/ork.js'></script>
<title>Kakapo : ColdFusion Exception Manager</title>
</head><body>
<div id="alertbox"><table onClick="hidealertbox()" cellpadding="0" cellspacing="0" border="0"><tr><td width="5"><img src="images/alrtlft.gif" /></td><td class="alertboxmsg"></td><td width="5"><img src="images/alrtrite.gif" /></td></tr></table></div>
<cfif StructKeyExists(session,'msg') and not StructKeyExists(session,'error')>
<script type="text/javascript">
	$("document").ready(function() { setTimeout('showmsg("<cfoutput>#session.msg#</cfoutput>")', 1000);  })
	<cfset StructDelete(session, "msg")>
</script>
</cfif>

<cfif StructKeyExists(session,'error')>
<script type="text/javascript">
	$("document").ready(function() { setTimeout('showerror("<cfoutput>#session.error#</cfoutput>")', 1000) })
	<cfset StructDelete(session, "error")>
</script>
</cfif>

<div id="fullbox">
<cfif YesNoFormat(url.header)>
	<cfset headerdislpay = "block">
<cfelse>
	<cfset headerdislpay = "none">
</cfif>

<div id="header" style="display:<cfoutput>#headerdislpay#</cfoutput>"><a href="index.cfm"><img src="images/kakapo_green.png" style="float:left" title="Kakapo : ColdFusion Exception Manager" border="0" /></a>

<div style="float:right">
<img src="images/import.png" id="implink" class="alert" original-title="Import" style="margin-right:3px; cursor:pointer" />
<a href="index.cfm?action=settings"><img class="alert" src="images/settings.png" original-title="Settings" border="0" style="margin-right:3px" /></a>
<a href="login.cfm?path=logout"><img class="alert" src="images/logout.png" original-title="Log Out" border="0" style="margin-right:3px" /></a>
</div>
<div style="clear:both"></div>
</div>

<!--- import --->
<div id="imp">
<img src="images/close_box.gif" align="right" id="impclose" style="cursor:pointer" />
<form style="clear:both; padding-left:10px" enctype="multipart/form-data" action="index.cfm?action=import" method="post">
<input type="file" size="10" name="file" class="text" /> <input type="submit" class="button" value="Import" />
</form>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$('#impclose').click(function(e) {
		$('#imp').slideUp('fast')
	});
	$('#implink').click(function(e) {
		$('#imp').slideDown('slow')
	});
});
</script>