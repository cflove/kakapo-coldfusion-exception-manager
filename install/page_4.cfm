<!--- ********************************************************************* --->
<!--- Kakapo 0.36. Released under the GNU licenses 3                        --->
<!--- Saman W Jayasekara (sam@cflove.org) http://cflove.org                 --->
<!--- Last Update : August 22, 2011                                         --->
<!--- ********************************************************************* --->

<cfparam name="form.dbtype" default="">

<cfset createTable = "0">
<!--- ********************************************************************* --->
<!--- Create Data Table                                                     --->
<!--- ********************************************************************* --->
<cftry>
<cfquery name="chk" datasource="#cfusion_decrypt(form.d,settingfile)#" username="#cfusion_decrypt(form.u,settingfile)#" password="#cfusion_decrypt(form.p,settingfile)#">
	select bugid from #kakapo.table# where bugid = 1
</cfquery>
	<cfcatch>
		<cfset createTable = "1">
	</cfcatch>
</cftry>

<cfswitch expression="#form.dbtype#">
	<cfcase value="mssql">
		<cfif val(createTable)>
			<!--- create mssql database ---->
			<cfquery name="create" datasource="#cfusion_decrypt(form.d,settingfile)#" username="#cfusion_decrypt(form.u,settingfile)#" password="#cfusion_decrypt(form.p,settingfile)#">
				CREATE TABLE [#kakapo.table#](
					[bugid] [int] IDENTITY(1,1) NOT NULL,
					[code] [varchar](35) NULL,
					[errorid] [int] NULL,
					[template] [varchar](200) NULL,
					[message] [varchar](500) NULL,
					[line] [int] NULL,
					[type] [varchar](20) NULL,
					[etime] [datetime] NULL,
					[status] [tinyint] NULL,
					[viewed] [bit] NULL,
				 CONSTRAINT [PK_#kakapo.table#] PRIMARY KEY CLUSTERED  ([bugid] ASC)
				ON [PRIMARY]
				) 
				ON [PRIMARY]
			</cfquery>
		</cfif>
	</cfcase>
	<cfcase value="mysql">
		<cfif val(createTable)>
			<!--- create mysql database ---->
			<cfquery name="chk" datasource="#cfusion_decrypt(form.d,settingfile)#" username="#cfusion_decrypt(form.u,settingfile)#" password="#cfusion_decrypt(form.p,settingfile)#">
				CREATE TABLE #kakapo.table# (
				bugid 		int(11) NOT NULL AUTO_INCREMENT,
				code 		varchar(35) DEFAULT NULL,
				errorid 	int(11) DEFAULT NULL,
				template 	varchar(200) DEFAULT NULL,
				message 	varchar(500) DEFAULT NULL,
				line 		int(11) DEFAULT NULL,
				type 		varchar(20) DEFAULT NULL,
				etime 		datetime DEFAULT NULL,
				status 		tinyint(4) DEFAULT NULL,
				viewed 		bit(1) DEFAULT NULL,
				PRIMARY KEY (bugid)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;
			</cfquery>
		</cfif>
	</cfcase>
	<cfcase value="pssql">
		<cfif val(createTable)>
			<!--- create postgres database ---->
			<cfquery name="dropseq" datasource="#cfusion_decrypt(form.d,settingfile)#" username="#cfusion_decrypt(form.u,settingfile)#" password="#cfusion_decrypt(form.p,settingfile)#">
				DROP SEQUENCE IF EXISTS #kakapo.table#_seq
			</cfquery>
			
			<cfquery name="chk" datasource="#cfusion_decrypt(form.d,settingfile)#" username="#cfusion_decrypt(form.u,settingfile)#" password="#cfusion_decrypt(form.p,settingfile)#">
				CREATE SEQUENCE #kakapo.table#_seq;				
				CREATE TABLE #kakapo.table# (
				bugid 		INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('#kakapo.table#_seq'),
				code 		VARCHAR(35),
				errorid 	INTEGER,
				template	varchar(200),
				message		varchar(500),
				line		int,
				type		varchar(20),
				etime		date,
				status		smallint,
				viewed		bool
				);
			</cfquery>
		</cfif>
	</cfcase>
	<cfcase value="skip">
	</cfcase>
	<cfdefaultcase>
		<strong>Table cannot be created.</strong>
		<br /><br />Missing Database Type. 
	</cfdefaultcase>
</cfswitch>

<!--- ********************************************************************* --->
<!--- Save Login Page                                                       --->
<!--- ********************************************************************* --->

<cfoutput>
<cfsavecontent variable="pageContent">
[cfprocessingdirective pageEncoding="utf-8">
[cfset kakapo.username	= "#form.username#">
[cfset kakapo.password	= "#form.password#">
</cfsavecontent>
</cfoutput>

<cfset pageContent = trim(replace(pageContent,'[cf','<cf','all'))>

<cffile 
	action			= "write"
	nameconflict	= "overwrite"
	charset			= "utf-8"
	file			= "#ExpandPath('..\administrator\')#login_details.cfm"
	output			= "#pageContent#">

<cflocation addtoken="no" url="index.cfm?action=5">